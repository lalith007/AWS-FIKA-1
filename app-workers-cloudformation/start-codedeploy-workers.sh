#!/bin/bash
set -o pipefail
set -e
set -u

STACK_NAME="CodeDeployWorkers-$(date +%Y%m%d-%H%M%S)"
REGION="eu-west-1"

aws cloudformation validate-template \
    --template-body file://workers.yaml


STACK_ID=$( \
    aws cloudformation create-stack \
    --region ${REGION} \
    --stack-name ${STACK_NAME} \
    --template-body file://workers.yaml \
    --capabilities CAPABILITY_IAM \
    --parameters \
       ParameterKey=WorkerUserData,ParameterValue="$(base64 -w0 user-data-codedeploy.sh)" \
    | jq -r .StackId)

echo "https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=${REGION}#/stack/detail?stackId=${STACK_ID}"

aws cloudformation wait stack-create-complete --stack-name ${STACK_NAME} --region ${REGION}
