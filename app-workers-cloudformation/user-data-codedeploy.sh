#!/bin/bash
yum -y update
yum install -y ruby wget php70 httpd24 git
wget https://aws-codedeploy-eu-west-1.s3.amazonaws.com/latest/install
ruby install auto
service codedeploy-agent status
service httpd start
echo "<?php phpinfo(); ?>"  | tee /var/www/html/index.php
